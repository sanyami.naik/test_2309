package test_23_09;

import java.util.Scanner;

public class TeacherMenu {
    public static void main(String[] args) {
        TeacherDemo teacherDemo=new TeacherDemo();
        System.out.println("Enter the option .Enter 1 for creating table first");
        System.out.println("1 for Creating and inserting values");
        System.out.println("2 for Displaying table values");
        System.out.println("3 for updating the salary by a particluar amount");
        System.out.println("4 for deleting the teacher tuple by id");
        System.out.println("5 for city==mumbai and salary >10000");
        System.out.println("6 for Sorting Descending by lastName");


        while(true)
        {
            System.out.println("Enter the option you want");
            Scanner sc=new Scanner(System.in);
            int op=sc.nextInt();


        switch(op){
            case 1:
                teacherDemo.createTable();
                break;
            case 2:
                teacherDemo.displayData();
                break;

            case 3:
                teacherDemo.updateValue();
                break;

            case 4:
                teacherDemo.deleteValue();
                break;

            case 5:
                teacherDemo.conditionDisplay();
                break;

            case 6:
                teacherDemo.SortByLastname();
                break;

        }


        }
    }
}
