package test_23_09;

import java.sql.*;
import java.util.Scanner;

public class TeacherDemo {

    int id;
    String fName;
    String lName;
    int salary;
    int flatno;
    String HouseName;
    String city;
    int pincode;
    Scanner scanner = new Scanner(System.in);

    Connection connection;
    Statement statement;
    ResultSet resultSet;


    public void createTable() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher", "root", "Root@12");

            Statement statement = connection.createStatement();

            statement.execute("create table teacherinfo(id int,fname varchar(20),lname varchar(20),salary int,flatno int,Housename varchar(20),city varchar(20),pincode int);");

            statement.execute("insert into teacherinfo values(4,'Sanyami','Naik',30000,104,'house1','Pune',12345);");
            statement.execute("insert into teacherinfo values(5,'Salonii','Deshmukh',500,101,'house3','Mumbai',12565);");
            statement.execute("insert into teacherinfo values(6,'Divya','Agarwal',1234,102,'house7','Delhi',34345);");
            statement.execute("insert into teacherinfo values(9,'Zeba','Faniband',78300,100,'house2','Mumbai',12335);");
            statement.execute("insert into teacherinfo values(10,'Harita','Nair',30000,103,'house9','Mumbai',16795);");
            statement.execute("insert into teacherinfo values(1,'Sanskruti','Pathane',306430,108,'house6','Pune',12345);");


        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println("The table has been created successfully");
    }


    public void displayData() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher", "root", "Root@12");

            Statement statement = connection.createStatement();

            System.out.println("The teachers present in database are");
            resultSet = statement.executeQuery("Select * from teacherinfo;");
            while (resultSet.next()) {
                System.out.println(
                        "id=" + resultSet.getInt(1) +
                                ", fName='" + resultSet.getString(2) + '\'' +
                                ", lName='" + resultSet.getString(3) + '\'' +
                                ", salary=" + resultSet.getInt(4) +
                                ", flatno=" + resultSet.getInt(5) +
                                ", HouseName='" + resultSet.getString(6) + '\'' +
                                ", city='" + resultSet.getString(7) + '\'' +
                                ", pincode=" + resultSet.getInt(8));

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateValue() {
        System.out.println("Enter the amount of salary you want to increase ");
        int increaseSalray = scanner.nextInt();

        try {
            System.out.println("The updated salary by " + increaseSalray + "is :");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher", "root", "Root@12");
            Statement statement = connection.createStatement();
            statement.execute("Update teacherinfo set salary=salary+" + increaseSalray + ";");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println("The salaries of all the teachers have been updated!!");

    }

    public void deleteValue() {
        System.out.println("Enter the id of the teacher you want to delete ");
        int id = scanner.nextInt();

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher", "root", "Root@12");
            Statement statement = connection.createStatement();
            statement.execute("Delete from teacherinfo where id=" + id + ";");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("The teacher with " + id + " has been deleted succesfully");


    }


    public void conditionDisplay() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher", "root", "Root@12");
            Statement statement = connection.createStatement();

            resultSet = statement.executeQuery("select * from teacherinfo where salary>=10000 && city='Mumbai';");
            System.out.println("The teachers with salary greater than 10000 and city mumbai is");
            while (resultSet.next()) {

                System.out.println(
                        "id=" + resultSet.getInt(1) +
                                ", fName='" + resultSet.getString(2) + '\'' +
                                ", lName='" + resultSet.getString(3) + '\'' +
                                ", salary=" + resultSet.getInt(4) +
                                ", flatno=" + resultSet.getInt(5) +
                                ", HouseName='" + resultSet.getString(6) + '\'' +
                                ", city='" + resultSet.getString(7) + '\'' +
                                ", pincode=" + resultSet.getInt(8));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


    public void SortByLastname()

    {

        System.out.println("The data after sorting by last name is ");
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher", "root", "Root@12");
            Statement statement = connection.createStatement();
            resultSet=statement.executeQuery("Select * from teacherinfo Order By Lname DESC;");
            while (resultSet.next()) {

                System.out.println(
                        "id=" + resultSet.getInt(1) +
                                ", fName='" + resultSet.getString(2) + '\'' +
                                ", lName='" + resultSet.getString(3) + '\'' +
                                ", salary=" + resultSet.getInt(4) +
                                ", flatno=" + resultSet.getInt(5) +
                                ", HouseName='" + resultSet.getString(6) + '\'' +
                                ", city='" + resultSet.getString(7) + '\'' +
                                ", pincode=" + resultSet.getInt(8));
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}




